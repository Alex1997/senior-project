var express = require('express');
var router = express.Router();
var passport = require('passport')
var user = require('../Services/user.Service');
var auth = require('../Services/authentication.Service')

/* GET users listing. */
router.get('/get/:username', function (req, res, next) {
  var username = req.params.username;
  console.log(username)
  user.findOne({ username: username }, (err, result) => {
    if (err) {
      console.log(err);
      res.status(500);
      res.send(err);
    }
    else {
      if (result == null) {
        res.status(404);
        res.send();
      }
      else {
        res.send(result);
      }
    }
  })
});
router.get('/getbyid/:id', function (req, res, next) {
  var id = req.params.id;
  user.findById(id, (err, result) => {
    if (err) {
      console.log(err);
      res.status(500);
      res.send(err);
    }
    else {
      if (result == null) {
        res.status(404);
        res.send();
      }
      else {
        res.send(result);
      }
    }
  })
});
router.get('/current', (req, res, next) => {
  console.log("stuff");
  res.send(req.user)
});
router.post('/updatepassword',auth,(req,res,next) => {
  var oldpassword = req.body.oldpassword
  var newpassword = req.body.newpassword
  user.findById(req.user._id).then(function(sanitizedUser){
    if (sanitizedUser){
        sanitizedUser.changePassword(oldpassword,newpassword, function(){
            sanitizedUser.save();
            res.status(200).json({message: 'password change successful'});
        });
    } else {
        res.status(500).json({message: 'This user does not exist'});
    }
    },function(err){
        console.error(err);
    })
})
router.post('/', (req, res, next) => {
  var incUser = req.body.user;
  user.register(new user({
    username: incUser.username,
    email: incUser.email,
    name: incUser.name,
    age: incUser.age,
    created: new Date()
  }), incUser.password).then((res) => {

  }).then(()=> {
    console.log('user registered!');
    res.status(200);
    return res.send({message:"ok"});
  }).catch((err) => {
    console.log('error while registering user!');
    console.log(err)
    res.status(500);
    return res.send(err);
  });

})

router.post('/login', passport.authenticate('local'), (req, res, next) => {

  res.send(req.user)
});

module.exports = router;
