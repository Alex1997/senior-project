export class RegisterUser {
    username: string;
    password: String;
    email: string;
    name: string;
    age: Number;
}
