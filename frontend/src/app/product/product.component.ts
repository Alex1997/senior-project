import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ProductService } from '../product.service';
import { DataSource } from '@angular/cdk/collections';
import { Observable } from 'rxjs';
import { Product } from '../models/product.Model';
import { Router, ActivatedRoute } from '@angular/router';
import { MatPaginator } from '@angular/material';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit, AfterViewInit {


  constructor(private http: HttpClient, private productService: ProductService, private route: ActivatedRoute) { }
  categories: any;
  length: any;
  name;
  httpparams: HttpParams;
  products: Observable<Product[]>;
  filter(category: string) {
    this.httpparams = this.httpparams.set('category', '{exact}' + category);
    this.query();
  }
  search(name: string) {
    this.httpparams = this.httpparams.set('name', name);
    console.log(name);
    this.query();
  }
  query() {

    this.products = this.productService.query(this.httpparams);
    this.categories = this.productService.getCategories();
  }
  ngOnInit() {
    this.httpparams = new HttpParams();
    if (this.route.snapshot.queryParams['name']) {
      this.httpparams = this.httpparams.set('name', this.route.snapshot.queryParams['name']);
    }
    this.query();

  }
  ngAfterViewInit() {
  }
}
