import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { ProductService } from '../product.service';
import { User } from '../models/User.Model';
import { Product } from '../models/product.Model';
import { HttpClient, HttpParams } from '@angular/common/http';
@Component({
  selector: 'app-user-items',
  templateUrl: './user-items.component.html',
  styleUrls: ['./user-items.component.css']
})
export class UserItemsComponent implements OnInit {

  constructor(private userService: UserService, private productService: ProductService, private http: HttpClient) { }
  products: any;
  httpparams: any;
  delete(id: string) {
    this.productService.delete(id).subscribe(val => {
      this.products = this.productService.query(this.httpparams);
    }
    );
  }
  ngOnInit() {
    this.userService.current().subscribe(user => {
      this.httpparams = new HttpParams().set('seller', '{exact}' + user._id);
      this.products = this.productService.query(this.httpparams);
    });
  }

}
