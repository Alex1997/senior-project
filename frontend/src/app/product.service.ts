import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Product } from './models/product.Model';
@Injectable({
  providedIn: 'root'
})
export class ProductService {
  readonly ROOT_URL = 'http://localhost:3000/products';
  constructor(private http: HttpClient) { }
  getById(id: string) {
    return this.http.get<Product>(this.ROOT_URL + `/single/${id}`, { withCredentials: true });
  }

  add(product: Product) {
    return this.http.post<string>(this.ROOT_URL + '/', { product: product }, { withCredentials: true });
  }

  update(product: Product) {
    return this.http.post(this.ROOT_URL + '/update', { product: product }, { withCredentials: true });
  }

  getCategories() {
    return this.http.get<string[]>(this.ROOT_URL + '/categories', { withCredentials: true });
  }

  query(params: HttpParams) {
    return this.http.get<Product[]>(this.ROOT_URL + '/query', { params: params });
  }
  delete(id: string) {
    return this.http.delete(this.ROOT_URL + `/${id}`, { withCredentials: true });
  }
}
